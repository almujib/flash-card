var data = {};

data.PAPERS_SIZE = (() => require("../PAPERS_SIZE.json"))();

data.fetch = (index=undefined) => {
	const buffer = require("../data.json").data;
	return (index || index == 0) ? buffer[index] : buffer; 
}

data.settings = () => require("../data.json").settings;

data.get_size = str => data.PAPERS_SIZE[str];

module.exports = data;