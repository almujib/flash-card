// GET SIZE
const size = (() => {
	const w = document.getElementById("w").textContent.split(" ")[1];
	const h = document.getElementById("h").textContent.split(" ")[1];
	return {
		w: w,
		h: h
	}
})();

// SET MAIN SIZE
(() => {
	const main = document.getElementById("main");
	main.style.width = size.w + "cm";
	main.style.height = size.h + "cm";
})();

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

// SET PRINT BUTTON
document.getElementById("print").addEventListener("click", () => {
	const main = document.getElementById("main");
	const printWindow = window.open("", "", "");
	printWindow.document.write(main.cloneNode(true).outerHTML);
	printWindow.document.write(`<link rel="stylesheet" href="stylesheets/style.css">`);
	printWindow.document.title = makeid(5) + " " + document.getElementById("active").textContent;
	
	setTimeout(function(){printWindow.print();printWindow.close();},10);
      

      return true;

});