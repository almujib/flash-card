var express = require('express');
var router = express.Router();

const data = require('../models/Data');

/* GET home page. */
router.get('/', function(req, res, next) {
  var context = {
    title: 'Flash Card'
  }
  res.render('index', context);
});

router.get('/main', function(req, res, next) {
  index = req.query.p ? (req.query.p - 1) : 0;
  var buffer = {
    data: data.fetch(index),
    settings: data.settings()
  }

  var context = {
    title: 'Flash Card - Main',

    str_size: buffer.settings.size,
    size: data.get_size(buffer.settings.size),

    pagination: {
      active: req.query.p,
      length: data.fetch().length
    },

    data: buffer.data
  }

  res.render('main', context);
});

module.exports = router;
